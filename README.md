## SSM - Secure Service Manager

Предназначен для безопасного выполнения команд на серверах.

# Как скачать один файл, пример:

https://gitlab.com/biit/ssm/raw/master/modules/www/create

# Краткая инструкция по работе с git-ом.

Клонируем репозиторий (если ещё не существует):

    git clone git@gitlab.com:biit/ssm.git

Создаем новую ветку для разработки:

    git checkout -b 'имя ветки' origin/master

Вносим необходимые изменения, проверяем свою работу:

    git status
    git diff

Фиксируем изменения в ветке:

    git status
    git add . | git rm ... | git checkout -- file
    git commit

Выкладываем изменения на ревью:

    git push origin 'имя ветки'


