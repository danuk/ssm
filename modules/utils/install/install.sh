#!/bin/bash

#v.1.1.3
#Install.sh written by Danuk modified by SMV

WEB_INSTALL=0
MAIL_INSTALL=0
SQL_INSTALL=0

ARG=$1

if [ -z $ARG ]; then
	echo "USAGE: $0 (mysql|www|mail|all)"
	exit 1
fi

for i in $@
do
	if [ $i == "all" ]; then
		SQL_INSTALL=1
		WEB_INSTALL=1
		MAIL_INSTALL=1
		break
	else
		[ $i == "mysql" ] && SQL_INSTALL=1
		[ $i == "www" ] && WEB_INSTALL=1
		[ $i == "mail" ] && MAIL_INSTALL=1
	fi
done


echo "Settings home and root directory access."
chmod 700 /root
chmod 711 /home

echo "Creating staff directory (if necessary)"
test -d /home/staff && chmod 700 /home/staff
test -d /home/staff || mkdir -m 0700 /home/staff

echo "Creating sources"
id sources 2>/dev/null || useradd -d /usr/src sources

echo "Creating software directory"
test -d /usr/src/software || mkdir -m 0700 /usr/src/software


if [ ! -d "/home/clients" ]; then
	echo "Creating clients directory."
	mkdir -m 0711 /home/clients
fi

apt-get install vim lftp mc curl locales-all unrar-free unzip psmisc rsync pwgen mailutils acct screen less zip flex quota quotatool libgd-graph-perl libdbi-perl libxml2-dev libbz2-dev libcurl4-openssl-dev libjpeg-dev libpng12-dev libfreetype6-dev libmcrypt-dev libmhash-dev libreadline-dev libxslt1-dev libauthen-pam-perl libpam-dev bzip2 lsb-release kernel-package libncurses5-dev libreadline-dev fetchmail git build-essential telnet whois dialog apt-utils perl-doc

# Install Perl ACK
curl http://beyondgrep.com/ack-2.14-single-file > /bin/ack && chmod 0755 /bin/ack

#dpkg-reconfigure locales

a=`pwd`;

if [ ! -d "/opt/ssm" ]; then

	# Install data
	echo "Unpacking data.tgz..."
	cd / && tar -xzf $a/data2.tgz
	# Install ssmc
	cd /usr/src/software/ssmc && make && make install && cd .. && rm -r ssmc-1.0.1 ssmc

fi

ip=`ifconfig eth0 | grep 'inet addr' | sed 's/\s*inet addr://;s/\s*Bcast:.*//'`;

if [ ! -d "/opt/proftpd" ]; then
	# Install Proftpd
	test -d /usr/src/software/proftpd && cd /usr/src/software/proftpd && ./compile.it
	test -f /etc/proftpd.conf && sed -i "s/IP_ADDR/${ip}/" /etc/proftpd.conf
fi

if [ $SQL_INSTALL == 1 ]; then
	echo "Installing SQL-packet..."

	test -d /home/clients/databases || mkdir -m 0711 /home/clients/databases
	test -d /opt/mysql || mkdir -m 0755 /opt/mysql
	test -d /etc/mysql && rm -rf /etc/mysql
	# Install MySQL
	test -d /usr/src/software/mysql && cd /usr/src/software/mysql && ./compile.it
	# Install DBD
	test -d /usr/src/software/DBD-mysql && cd /usr/src/software/DBD-mysql && ./compile.it && make && make install
	test -d /usr/src/software/DBD-mysql-pm && cd /usr/src/software/DBD-mysql-pm && ./install.sh

	sed -i "s/IP_ADDR/${ip}/" /opt/mysqlmgr/bin/create_db
	
	ln -snf /opt/mysql/current/bin/* /usr/local/bin/
	ln -snf /opt/mysql/current/libexec/mysqld /usr/local/bin/
	ln -snf /opt/mysqlmgr/bin/mysql_init /usr/local/bin/
fi

if [ $WEB_INSTALL == 1 ]; then
	echo "Installing WEB-packet..."
	
	test -d /home/clients/websites || mkdir -m 0711 /home/clients/websites
	test -d /home/clients/websites/trash || mkdir -m 0711 /home/clients/websites/trash

	groups dpanel 2>/dev/null || groupadd -g 200 dpanel
	id dpanel 2>/dev/null || useradd -u 200 -g 200 -d /home/dpanel -m dpanel
	addgroup --gid 110 proc
	addgroup --gid 155 virtwww
	id www 2>/dev/null || useradd -u 15 -d /opt/apache -s /bin/false www
	id webstats 2>/dev/null || useradd -d /home/webstats -m webstats
	test -d /home/webstats/output || (mkdir /home/webstats/output && chown webstats: /home/webstats/output)
	test -d /home/webstats/history || (mkdir /home/webstats/history && chown webstats: /home/webstats/history)
	test -d /var/log/httpd/virtwww 2>/dev/null || mkdir -m 0711 -p /var/log/httpd/virtwww
	test -d /tmp/.php || mkdir -m 1733 /tmp/.php

	test -d /opt/suexec || mkdir -m 0755 /opt/suexec
	test -d /opt/apache || mkdir -m 0755 /opt/apache
	test -d /opt/php || mkdir -m 0755 /opt/php
	# Install suExec
	test -d /usr/src/software/suexec/ && cd /usr/src/software/suexec/ && make && make install
	# Install Apache
	test -d /usr/src/software/httpd && cd /usr/src/software/httpd && ./compile.it
	# Install PHP
    test -d /usr/src/software/php && cd /usr/src/software/php && ./compile.it


	test -f /opt/apache/config/conf/httpd.conf && sed -i "s/0\.0\.0\.0/${ip}/" /opt/apache/config/conf/httpd.conf
	test -f /opt/apache/config/conf/extra/httpd-ssl.conf && sed -i "s/0\.0\.0\.0/${ip}/" /opt/apache/config/conf/extra/httpd-ssl.conf

	test -f /opt/apache/current/bin/apachectl && ln -snf /opt/apache/current/bin/apachectl /usr/local/bin/
	test -f /opt/apache/current/bin/htpasswd && ln -snf /opt/apache/current/bin/htpasswd /usr/local/bin/
	test -f /opt/php/current/bin/php && ln -snf /opt/php/current/bin/php /usr/local/bin/

	ps -C httpd &>/dev/null && apachectl restart || apachectl start

    `a2enmod suexec`;
    `a2enmod rewrite`;

fi

if [ $MAIL_INSTALL == 1 ]; then
	echo "Installing MAIL-packet..."

	apt-get -f install postfix dovecot-pop3d dovecot-imapd

	mkdir -m 0711 /home/clients/mail
	chown root:dovecot /usr/lib/dovecot/deliver
	chmod 4750 /usr/lib/dovecot/deliver
	# Install webmail:
	create-virtwww mail webmail
	su - -c 'cd webmail && tar -xzf /usr/src/software/webmail.tgz' w_mail

	ln -snf /opt/php/current/bin/php /usr/local/bin/

	/etc/init.d/postfix restart
	/etc/init.d/dovecot restart

fi

test -f /etc/network/iptables || touch /etc/network/iptables

#test -f /usr/bin/gcc && chmod 700 /usr/bin/gcc 
#test -f /usr/bin/g++ && chmod 700 /usr/bin/g++ 
#test -f /usr/bin/cpp && chmod 700 /usr/bin/cpp
chmod 700 /usr/bin/last
chmod 700 /usr/bin/passwd

ln -snf /usr/bin/perl /usr/local/bin/

if [ -d "/space" ]; then
sed -i 's/\/space\s*ext(3|4)\s*defaults/\/space\t\text3\trw,nosuid,noatime,usrquota,grpquota/' /etc/fstab
mount | grep /space | grep noatime || mount -o remount,nosuid,noatime,usrquota,grpquota /space
else
echo "Warning: /space not exist!";
fi


