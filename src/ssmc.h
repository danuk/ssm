#include "defines.h"

uint16_t to_log(uint16_t, char *, ...);

void upcase(char* str)
{
        register uint16_t i;

        for(i=0; str[i]!='\0'; i++)
                str[i] = toupper(str[i]);

        return;
}

void init_config(char * cmd)
{
	char * config = malloc(MAX_PATH_LEN);
	FILE * fd;
	char * value;
	uint16_t buff_size = 512;
	char buff[buff_size];

	strcpy(config, PATH_CONF);
	strcat(config, "/env.cfg");

	if (access(config, R_OK) == 0) { //Configuring enveronment
		if ((fd = fopen(config, "r")) == NULL) {
			//TODO: print error to log
		}
		
		while ((fgets(buff, buff_size-1, fd))!= NULL) {
			if (buff[0] == '#' || (value = strstr(buff, "=")) == NULL)
				continue;
			value[0] = '\0';
			value++;
			value[strlen(value)-1] = '\0';

			setenv(buff, value, 1);
		}

		fclose(fd);
	}

	sprintf(config, "%s%c%s%s", PATH_CONF, '/', cmd, ".cfg");

	if (access(config, R_OK) == 0) { //Configuring module
		char * ssm_env = malloc(128);

		if ((fd = fopen(config, "r")) != NULL) {
			while ((fgets(buff, buff_size-1, fd))!= NULL) {
				if (buff[0] == '#' || (value = strstr(buff, "=")) == NULL)
					continue;
	
				value[0] = '\0';
				value++;
				value[strlen(value)-1] = '\0';

				strcpy(ssm_env, "SSM_");
				strcat(ssm_env, buff);
				upcase(ssm_env);			
	
				setenv(ssm_env, value, 1);
			}

			fclose(fd);
		}

		free(ssm_env);
	}

	free(config);

	return;
}

int8_t get_exec(char * cmdo, char * exec_wargs[])
{
	register uint16_t i;
	uint16_t s;
	
	char * cmd = malloc(strlen(cmdo)+1);
	strcpy(cmd, cmdo);

	// Splitting SSH_ORIGINAL_COMMAND
	for (i = 0, s = 1; cmd[i+1]!='\0'; i++) {
		if (cmd[i] != ' ')
			continue;
		
		cmd[i]='\0'; // Cutting cmd string
	
		if (cmd[i+1] == ' ')
			continue;
		
		exec_wargs[s++] = cmd+i+1; // add to arg

		if (s == MAX_INT_ARGS-1) {
			to_log(LOG_ERR, "Too much arguments");
			return (-1);
		}
	}

	exec_wargs[s] = NULL;

	exec_wargs[0] = malloc(MAX_PATH_LEN);

	strcpy(exec_wargs[0], PATH_SCRIPTS);
	strcat(exec_wargs[0], "/");
	strcat(exec_wargs[0], cmd);

	struct stat ss;

	//Searching module and args
	for(i=1; i <= s; i++) {
		if (stat(exec_wargs[0], &ss) != 0) {
			to_log(LOG_ERR, "can't stat %s", exec_wargs[0]);
			return (-1);
		}

		if (S_ISDIR(ss.st_mode)) {
			strcat(exec_wargs[0], "/");
			strcat(exec_wargs[0], exec_wargs[i]);
		}
		else {
			if (access(exec_wargs[0], (R_OK|X_OK))==0) {
				return (s);
			}
			else {
				to_log(LOG_ERR, "%s is not executable", exec_wargs[0]);
				return (-1);
			}
		}
	}
	
	return (-1);
}

char * get_ssm_category(char * str)
{
	register uint16_t i;

	for (i = 0; str[i] != '\0'; i++) {

		if (str[i] == ' ') {
			char * tmp = malloc(i);
			strncpy(tmp, str, i);
			tmp[i] = '\0';
			return tmp;
		}
	}

	return NULL;
}

uint16_t to_log(uint16_t lvl, char * smsg, ...)
{
	uint16_t c;
	va_list vl;
	char * p, *sval, * msg;

	if ((msg = malloc(MAX_LOGMSG_LEN)) == NULL) {
#ifdef W_SYSLOG
		syslog(LOG_ERR, "can't alloc memory");
#else
		fprintf(stderr, "ERROR: can't alloc memory\n");
#endif
		return;
	}

	va_start(vl, smsg);

	for (p = smsg, c = 0; *p && c < MAX_LOGMSG_LEN; p++) {
		if (*p != '%') {
			msg[c++] = *p;
			continue;
          }
		
		switch(*++p) {
          case 'd':
               c += sprintf(msg+c, "%d", va_arg(vl, int));
               break;
          case 's':
               for (sval = va_arg(vl, char *); *sval; sval++)
                    msg[c++] = *sval;
                    break;
          default:
               break;
          }
	}

	msg[c] = '\0';

#ifdef W_SYSLOG
	syslog(lvl, "%s", msg);
#else
	if (lvl == LOG_ERR)
		fprintf(stderr, "ERROR: %s\n", msg);
	else if (lvl == LOG_INFO)
		printf("INFO: %s\n", msg);
#endif
	free(msg);
	return 0;
}

#ifdef W_SYSLOG
void exit_proc(void)
{
	closelog();
}
#endif

int8_t open2_exec(char * execs[], uint8_t pda[])
{
     uint32_t sout[2], serr[2];
     pid_t pid;

     if ((pipe(sout)) == -1 || (pipe(serr)) == -1)
          return 0;


     if ((pid = fork()) == -1)
          return 0;

     if (pid == 0) {
          close(sout[0]);
          close(serr[0]);

          dup2(sout[1], STDOUT_FILENO);
          dup2(serr[1], STDERR_FILENO);

          close(sout[1]);
          close(serr[1]);

          execv(execs[0], execs);

          exit (1);
     }

     close(sout[1]);
     close(serr[1]);

     pda[0] = sout[0];
     pda[1] = serr[0];

     return 1;
}
