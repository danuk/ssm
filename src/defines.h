#ifndef SSM_C
#define SSM_C

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <stdint.h>
#include <sys/stat.h>
#include <stdarg.h>
#include <unistd.h>
#include <fcntl.h>

#define MAX_INT_ARGS 128
#define EXEC_TIMEOUT 30
#define MAX_PATH_LEN 512
#define READ_BUFF_SIZE 2048
#define MAX_LOGMSG_LEN 1024
#define PATH_SCRIPTS "/opt/ssm/current"
#define PATH_CONF "/opt/ssm/conf"

#ifdef W_SYSLOG
#include <syslog.h>
#else
#define LOG_ERR 3
#define LOG_INFO 6
#endif

#define check_fd(fd) (fcntl(fd, F_GETFL, 0) == -1 ? 0 : 1)

#endif
