#include "ssmc.h"


int main(int argc, char *argv[])
{
#ifdef W_SYSLOG
	openlog(argv[0], LOG_PID, LOG_LOCAL0);

	if (atexit(exit_proc))
		to_log(LOG_ERR, "can't set atexit function");
#endif

#ifdef LOCK_GLOBAL
	/*exit if locking failed*/
	if (lockf(1, F_LOCK, 0) != 0) {
		to_log(LOG_ERR, "Locking failed");
		exit(4);
	}
#endif
	char * command, * category;
	char * exec_wargs[MAX_INT_ARGS];
	uint8_t darr[2];
	
	if (!(command = getenv("SSH_ORIGINAL_COMMAND"))) {
		to_log(LOG_ERR, "No Command");
		exit(1);
	}

	if (get_exec(command, exec_wargs) == -1) { // parse command and get executable command w. args
		to_log(LOG_ERR, "invalid command");
		exit(2);
	}

	if ((category = get_ssm_category(command)) == NULL) {
		to_log(LOG_ERR, "can't get SSM category");
		exit(5);
	}

	init_config(category);

	alarm(EXEC_TIMEOUT);
	
	if (open2_exec(exec_wargs, darr) != 1) { // if execution failed
		to_log(LOG_ERR, "open2_exec error");
		exit(6);
	}

	if (check_fd(darr[0]) == 0 || check_fd(darr[1]) == 0) {
		to_log(LOG_ERR, "stdout or stderr closed");
          exit(7);
	}

	void * buff;
	uint16_t rcount;
	
	if ((buff = malloc(READ_BUFF_SIZE)) == NULL) {
		to_log(LOG_ERR, "can't allocate memory for read answer from open2_exec");
          exit(8);
	}

	while((rcount = read(darr[0], buff, READ_BUFF_SIZE-1)) > 0) { // print all from open2_exec to STDOUT
		((char *) buff)[rcount] = '\0';
		printf("%s",(char *) buff);
     }

	if ((rcount = read(darr[1], buff, MAX_LOGMSG_LEN-1)) > 0) { // print first 1023b. of error message from open2_exec to STDERR
          ((char *) buff)[rcount] = '\0';
          fprintf(stderr, "%s",(char *) buff);
		to_log(LOG_ERR, (char *) buff);
     }
	
	exit(0);
}
